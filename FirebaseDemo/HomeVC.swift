//
//  HomeVC.swift
//  FirebaseDemo
//
//  Created by flydino on 12/4/20.
//

import UIKit
import Firebase
import GoogleSignIn

class HomeVC: UIViewController {

    override func viewDidLoad() {
        super.viewDidLoad()
        print(Auth.auth().currentUser?.email)
    }
    
    @IBAction func didClickLogout(_ sender: Any) {
        do {
            try Auth.auth().signOut()
            if let vc = self.storyboard?.instantiateViewController(identifier: "ViewController") {
                vc.modalPresentationStyle = .fullScreen
                self.present(vc, animated: false, completion: nil)
            }
        } catch (let error) {
            print(error.localizedDescription)
        }
    }
}
