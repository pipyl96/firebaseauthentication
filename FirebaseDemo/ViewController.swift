//
//  ViewController.swift
//  FirebaseDemo
//
//  Created by flydino on 12/4/20.
//

import UIKit
import Firebase
import MBProgressHUD
import GoogleSignIn
import FBSDKLoginKit
import FBSDKCoreKit

class ViewController: UIViewController {

    @IBOutlet weak var emailTF: UITextField!
    @IBOutlet weak var passwordTF: UITextField!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        GIDSignIn.sharedInstance()?.delegate = self
        
//        let loginButton = FBLoginButton()
//        loginButton.delegate = self
//        loginButton.center = view.center
//        view.addSubview(loginButton)
//
//        if let token = AccessToken.current, !token.isExpired {
//            // User is logged in, do work such as go to next view controller.
//            print("Login fb success")
//        }
    }
    
    private func showHomeVC() {
        DispatchQueue.main.async {
            if let vc = self.storyboard?.instantiateViewController(withIdentifier: "HomeVC") {
                vc.modalPresentationStyle = .fullScreen
                self.present(vc, animated: true, completion: nil)
            }
        }
    }

    @IBAction func didClickRegister(_ sender: Any) {
        MBProgressHUD.showAdded(to: self.view, animated: true)
        Auth.auth().createUser(withEmail: emailTF.text!, password: passwordTF.text!) { [weak self] (result, error) in
            guard let self = self else { return }
            MBProgressHUD.hide(for: self.view, animated: true)
            
            if let error = error {
                print(error.localizedDescription)
            } else {
                print("Register successed")
            }
        }
    }
    
    @IBAction func didClickLogin(_ sender: Any) {
        Auth.auth().signIn(withEmail: emailTF.text!, password: passwordTF.text!) { [weak self] (result, error) in
            guard let self = self else { return }
            MBProgressHUD.hide(for: self.view, animated: true)
            
            if let error = error {
                print(error.localizedDescription)
            } else {
                self.showHomeVC()
            }
        }
    }
    
    @IBAction func didClickLoginGoogle(_ sender: Any) {
        GIDSignIn.sharedInstance()?.presentingViewController = self
        GIDSignIn.sharedInstance().signIn()
    }
    
    @IBAction func didClickLoginFacebook(_ sender: Any) {
        LoginManager().logIn(permissions: [], viewController: self) { (result) in
            switch result {
            case .success(let granted, let declined, let token):
                let credential = FacebookAuthProvider.credential(withAccessToken: AccessToken.current!.tokenString)
                Auth.auth().signIn(with: credential) { [weak self] (result, error) in
                    guard let self = self else { return }
                    if let error = error {
                        print("Login google firebase error \(error)")
                        return
                    }
                    self.showHomeVC()
                }
                return
            case .failed(let error):
                return
            case .cancelled:
                return
            }
        }
    }
}

extension ViewController: GIDSignInDelegate {
    func sign(_ signIn: GIDSignIn!, didSignInFor user: GIDGoogleUser!, withError error: Error!) {
        guard let user = user else { return }
        print("Login google success \(String(describing: user.profile.email))")
        
        guard let authentication = user.authentication else { return }
        let credential = GoogleAuthProvider.credential(withIDToken: authentication.idToken,
                                                          accessToken: authentication.accessToken)
        
        Auth.auth().signIn(with: credential) { [weak self] (result, error) in
            guard let self = self else { return }
            if let error = error {
                print("Login google firebase error \(error)")
                return
            }
            self.showHomeVC()
        }
    }
    
    func sign(_ signIn: GIDSignIn!, didDisconnectWith user: GIDGoogleUser!, withError error: Error!) {
        print("Login google error \(error.localizedDescription)")
    }
}

extension ViewController: LoginButtonDelegate {
    func loginButtonDidLogOut(_ loginButton: FBLoginButton) {
        
    }
    
    func loginButton(_ loginButton: FBLoginButton, didCompleteWith result: LoginManagerLoginResult?, error: Error?) {
        if let error = error {
            print(error.localizedDescription)
        } else {
            let credential = FacebookAuthProvider.credential(withAccessToken: AccessToken.current!.tokenString)
            Auth.auth().signIn(with: credential) { [weak self] (result, error) in
                guard let self = self else { return }
                if let error = error {
                    print("Login google firebase error \(error)")
                    return
                }
                self.showHomeVC()
            }
        }
    }
}
